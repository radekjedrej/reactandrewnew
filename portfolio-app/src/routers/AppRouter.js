import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import HomePage from '../components/HomePage'
import Portfolio from '../components/Portfolio'
import PortfolioSingle from '../components/PortfolioSingle'
import ContactPage from '../components/ContactPage'
import NotFoundPage from '../components/NotFoundPage'
import Header from '../components/Header'

const AppRouter = () => (
    <Router>
        <div>
            <Header />
            <Switch>
                <Route path="/" exact={true} component={HomePage} />
                <Route path="/portfolio" exact={true} component={Portfolio} />
                <Route path="/portfolio/:id" component={PortfolioSingle} />
                <Route path="/contact" component={ContactPage} />
                <Route path="*" component={NotFoundPage} />
            </Switch>
        </div>
    </Router>
)

export default AppRouter