import React from 'react'
import { Link } from 'react-router-dom'


const PortfolioSingle = (props) => {
    return (
        <div>
            This is Portfolio Single Page {props.match.params.id}

            <Link to="/portfolio">Go Back</Link>
        </div>
    )
}

export default PortfolioSingle