const firstName = 'Mike Smith'

const getFirstName = (name) => {
    return name.split(' ')[0]
}

const getFirstNameShort = (name) => name.split(' ')[0] 

console.log(getFirstName(firstName));
console.log(getFirstNameShort(firstName));

const multiplier = {
    numbers: [2, 6, 12, 24],
    multiplyBy: 2,

    multiply() {
        return this.numbers.map(number => number * this.multiplyBy);
    }
}

console.log(multiplier.multiply());
