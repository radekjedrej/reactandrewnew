class VisibilityToggle extends React.Component {
    constructor(props) {
        super(props)

        this.handleToggleVisibility = this.handleToggleVisibility.bind(this)
        this.state = {
            visibility: false
        }
    }

    handleToggleVisibility() {
        this.setState((prevState) => {
            return {
                visibility: !prevState.visibility
            }
        })         
    }

    render() {
        return (
            <div>
                <h1>Visibility Toggle</h1>
                <button onClick={this.handleToggleVisibility}>{this.state.visibility ? 'Hide details' : 'Show details'}</button>
                {this.state.visibility && (
                <div><p>Hey, These are some details you can now see!</p></div>
                )}
            </div>
        )
    }
}

ReactDOM.render(<VisibilityToggle />, document.getElementById('app'))


// let toggleShow = false

// const showDetails = () => {
//     toggleShow = !toggleShow
//     renderView()
// }

// const app = document.getElementById('app')

// const renderView = () => {
//     const template = (
//         <div>
//             <h1>Visibility Toggle</h1>
//             <button onClick={showDetails}>{toggleShow ? 'Hide details' : 'Show details'}</button>
            // {toggleShow && (
            //     <div><p>Hey, These are some details you can now see!</p></div>
            // )}
            
//         </div>
//     )
    
    
//     ReactDOM.render(template, app)
// }

// renderView()