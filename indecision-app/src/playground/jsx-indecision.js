console.log('app is running')

const app = {
    title: 'Krwara masakra',
    subtitile: 'Film z 2019',
    options: []
}

const onFormSubmit = (e) => {
    e.preventDefault()

    const option = e.target.elements.option.value

    if (option) {
        app.options.push(option)
        e.target.elements.option.value = ""
        render()
    } 
}

const onRemoveAll = () => {
    app.options = []
    render()
}

const onMakeDecision = () => {
    const randomNum = Math.floor(Math.random() * app.options.length)
    const option = app.options[randomNum]
    console.log(option);
    
}

const appRoot = document.getElementById("app")

const render = () => {
    const template = (
        <div>
            <h1>{app.title}</h1>
            {app.subtitile && <p>{app.subtitile}</p>}
            {app.options.length > 0 ? <p>Here are your options</p> : <p>No options</p>}
            <button disabled={app.options.length === 0} onClick={onMakeDecision}>What should i do</button>
            <button onClick={onRemoveAll}>Remove All</button>
            <ol>
                { app.options.map((option, index) => <li key={index}>{option}</li>) }
            </ol>
            <form onSubmit={onFormSubmit}>
                <input type="text" name="option" />
                <button>Add Option</button>
            </form>
        </div>
    )
    ReactDOM.render(template, appRoot)
}

render()